
var path = require('path');
//var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var fs = require('fs');



/**
 * IMPORT express
 * instantiate router
 * instatiate app 
 */
var express = require('express');
var router = express.Router();
var app = express();

// IMPORT APP CONFIG
var config = require('./_config');

/**
 * IMPORT CONTROLLER FILES
 */
var routes = require('./routes/index');
var items = require('./routes/itemController');
var employees = require('./routes/employeeController');
var theatres = require('./routes/theatreController');
var theatreItems = require('./routes/theatreItemController');
var config = require('./_config.js');
var movies = require('./routes/movieController');
var theatreMovies = require('./routes/theatreMovieController');
var login = require('./routes/loginController');
var credentials = require('./routes/credentialController');
var seq = require('./routes/seqController'); // not needed, remove this later

var logdir = path.join(__dirname, config.log.dir[app.settings.env]);

/**
 * CODE LOGGER SETTINGS
 */
var winston = require('winston'); // code logger module - WINSTON
// logdir + logname + logext
winston.remove(winston.transports.Console); // remove console logger
var codeLogFile = path.join(logdir, config.log.code.logname[app.settings.env]) + config.log.ext[app.settings.env];
winston.add(winston.transports.File, {filename: codeLogFile}); //add file logger


/**
 * HTTP ACCESS LOGGER SETTINGS
 */
var logger = require('morgan'); // http logger module - MORGAN
// logdir + logname + logext
var httpLogFile = path.join(logdir, config.log.http.logname[app.settings.env]) + config.log.ext[app.settings.env];
var accessLogStream = fs.createWriteStream(httpLogFile, {flags: 'a'})
app.use(logger(config.log.http.logformat[app.settings.env],{stream: accessLogStream}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * 
 * SETUP URL ROUTES
 */

app.use('/', routes);
app.use('/items', items);
app.use('/employees', employees);
app.use('/theatres',theatres);
app.use('/movies', movies);
app.use('/theatreItems',theatreItems);
app.use('/theatreMovies',theatreMovies);
app.use('/login',login);
app.use('/credentials',credentials);
app.use('/nextseq',seq);

mongoose.connect(config.mongoURI[app.settings.env], function(err, res) {
  if(err) {
    console.log('Error connecting to the database. ' + err);
  } else {
    console.log('Connected to Database: ' + config.mongoURI[app.settings.env]);
  }
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
app.listen(8081);
console.log("Server started and listening at 8081");