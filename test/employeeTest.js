process.env.NODE_ENV = 'test';

var employeeModel = require("../models/employee");
var counterModel = require("../models/counter");
var credentialModel = require("../models/credential");
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "BEGIN EMPLOYEE CRUD TESTS");
/**
 * Unit Test Cases
 */
describe('****EMPLOYEE CRUD TESTS****', function () {
  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test GET all employees
   */
  it('1. POSITIVE: should list ALL employees on /employees/all GET', function (done) {
    chai.request(server)
      .get('/employees/all')
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      })
  });

  /**
   * test GET a single employee
   */
  it('2. POSITIVE: should list a SINGLE employee on /employees/employee_id GET', function (done) {
    chai.request(server)
      .get('/employees/' + testEmployeeId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        res.body.data._id.should.equal(testEmployeeId);
        done();
      });
  });

  it('3. NEGATIVE: should fail to list a non-existing employee on /employees/employee_id GET', function (done) {
    chai.request(server)
      .get('/employees/' + employee_doesnot_exist.employee_id)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  /**
   * test creating a new employee
   */
  it('4. POSITIVE: should add a SINGLE employee on /employees/add POST', function (done) {
    chai.request(server)
      .post('/employees/add')
      .send(newEmployee)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });

  /**
   * test updating a single employee 
   */
  it('5. POSITIVE: should update a SINGLE employee on /employees/update POST', function (done) {
    chai.request(server)
      .post('/employees/update')
      .send(updateEmployee)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.employeeId.should.equal(updateEmployee.employeeId);
        done();
      });
  });

  /**
   * test de-activating a single employee 
   */
  it('6. POSITIVE: should delete a SINGLE employee on /employees/delete POST', function (done) {
    chai.request(server)
      .post('/employees/delete')
      .send(employee_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.employeeId.should.equal(employee_exists.employee_id);
        done();
      });
  });


  it('7. NEGATIVE: should fail to delete a non-existing employee on /employees/delete POST', function (done) {
    chai.request(server)
      .post('/employees/delete')
      .send(employee_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});

var testEmployeeId = "E00000";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  // function to drop collection before each test case
  dropCollection();
  var employee = new employeeModel({
    "_id": testEmployeeId,
    "companyName": "tcs",
    "firstName": "ABC",
    "lastName": "XYZ",
    "userName": "ABC_XYZ",
    "designation": "supervisor",
    "role": "supervisor",
    "homeAddress_houseNumber": "502",
    "homeAddress_street1": "Sai Kota Serene,Vaishali Enclave",
    "homeAddress_street2": "Madinaguda",
    "homeAddress_landmark": "besise Swagath Grand",
    "homeAddress_city": "Hyderabad",
    "homeAddress_state": "Telangana",
    "homeAddress_country": "India",
    "homeAddress_zipcode": "500050",
    "mobileNumber": "7702277874",
    "landlineNumber": "0891-581024",
    "status": "Active"
  });
  employee.save(function (err) {
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollection();
  done();
}

function dropCollection() {
  employeeModel.collection.drop();
  counterModel.collection.drop();
  credentialModel.collection.drop();
}


var updateEmployee = {
  "employeeId": testEmployeeId,
  "companyName": "AQUA",
  "firstName": "Neelima",
  "lastName": "M",
  "userName": "neelimam",
  "designation": "supervisor",
  "role": "supervisor",
  "homeAddress_houseNumber": "502",
  "homeAddress_street1": "Sai Kota Serene,Vaishali Enclave",
  "homeAddress_street2": "Madinaguda",
  "homeAddress_landmark": "besise Swagath Grand",
  "homeAddress_city": "Hyderabad",
  "homeAddress_state": "Telangana",
  "homeAddress_country": "India",
  "homeAddress_zipcode": "500050",
  "mobileNumber": "7702277874",
  "landlineNumber": "0891-581024",
  "status": "Active"
};


var newEmployee = {
  "companyName": "tcs",
  "firstName": "ABC",
  "lastName": "XYZ",
  "userName": "ABC_XYZ",
  "designation": "supervisor",
  "role": "supervisor",
  "homeAddress_houseNumber": "502",
  "homeAddress_street1": "Sai Kota Serene,Vaishali Enclave",
  "homeAddress_street2": "Madinaguda",
  "homeAddress_landmark": "besise Swagath Grand",
  "homeAddress_city": "Hyderabad",
  "homeAddress_state": "Telangana",
  "homeAddress_country": "India",
  "homeAddress_zipcode": "500050",
  "mobileNumber": "1234567890",
  "landlineNumber": "0891-581024",
  "status": "Active"
};

var employee_exists = { "employee_id": testEmployeeId };
var employee_doesnot_exist = { "employee_id": "abc1234" };