process.env.NODE_ENV = 'test';

var theatreItemModel = require("../models/theatreItem");

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "Logging Theatre Item Test CRUD");
/**
 * Unit Test Cases
 */
describe('****THEATRE-ITEM CRUD TESTS****', function () {

  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test GET all theatres
   */
  it('1. POSITIVE: should list ALL theatres on /theatreItems/all GET', function (done) {
    chai.request(server)
      .get('/theatreItems/all')
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      })
  });

  /**
   * test GET a single theatre
   */
  it('2. POSITIVE: should list a SINGLE theatre on /theatreItems/theatreItem_id GET', function (done) {
    chai.request(server)
      .get('/theatreItems/' + testTheatreItemId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        res.body.data._id.should.equal(testTheatreItemId);
        done();
      });
  });

  it('3. NEGATIVE: should fail to list a non-existing theatre on /theatreItems/theatreItem_id GET', function (done) {
    chai.request(server)
      .get('/theatreItems/' + theatreItem_doesnot_exist.theatre_id)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  /**
   * test creating a new theatre
   */
  it('4. POSITIVE: should add a SINGLE theatre on /theatreItems/add POST', function (done) {
    chai.request(server)
      .post('/theatreItems/add')
      .send(newTheatre)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.theatreItemId.should.not.be.empty;
        res.body.data.theatreItemId.should.equal("T00001I00001");
        done();
      });
  });

  /**
   * test updating a single theatre 
   */
  it('5. POSITIVE: should update a SINGLE theatre on /theatreItems/update POST', function (done) {
    chai.request(server)
      .post('/theatreItems/update')
      .send(updateTheatreItem)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
         res.body.data.theatreItemId.should.not.be.empty;
        res.body.data.theatreItemId.should.equal(updateTheatreItem.theatreItemId);
        done();
      });
  });

  /**
   * test de-activating a single theatre 
   */
  it('6. POSITIVE: should delete a SINGLE theatre on /theatreItems/delete POST', function (done) {
    chai.request(server)
      .post('/theatreItems/delete')
      .send(theatreItem_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
          res.body.data.theatreItemId.should.not.be.empty;

        res.body.data.theatreItemId.should.equal(theatreItem_exists.theatreItem_id);
        done();
      });
  });


  it('7. NEGATIVE: should fail to delete a non-existing theatre on /theatreItems/delete POST', function (done) {
    chai.request(server)
      .post('/theatreItems/delete')
      .send(theatreItem_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});

var testTheatreItemId = "T00000I00001";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  dropCollection();
  var theatre = new theatreItemModel({
   "_id": testTheatreItemId,
     "theatreId" :  "T00000",
    "theatreName":  "Sangam Sarat",
      "itemCode": "I00001", "itemName": "Pepsi", "supplierName": "Pepsi Co",
    "supplierContactName": "Ramesh",
        "supplierContactNumber":"7702299873","quantity": "100","perishable": false,"status":"Active"
  });
  theatre.save(function (err) {
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollection();
  done();
}

function dropCollection(){
  theatreItemModel.collection.drop();
}


var updateTheatreItem = {
   "theatreItemId" : "T00000I00001",
    "theatreId" :  "T00000",
    "theatreName":  "Sangam Sarat",
      "itemCode": "I00001", "itemName": "Pepsi", "supplierName": "Pepsi Co",
    "supplierContactName": "Ramesh",
        "supplierContactNumber":"7702299873","quantity": "100","perishable": false
};


var newTheatre = {
   "theatreId" :  "T00001",
    "theatreName":  "Sangam Sarat",
      "itemCode": "I00001", "itemName": "Pepsi", "supplierName": "Pepsi Co",
    "supplierContactName": "Ramesh",
        "supplierContactNumber":"7702299873","quantity": "100","perishable": false
};

var theatreItem_exists = { "theatreItem_id": testTheatreItemId };
var theatreItem_doesnot_exist = { "theatreItem_id": "abc1234" };
