process.env.NODE_ENV = 'test';

var theatreModel = require("../models/theatre");
var counterModel = require("../models/counter");
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "Logging Theatre Test CRUD");
/**
 * Unit Test Cases
 */
describe('****THEATRE CRUD TESTS****', function () {
  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test GET all theatres
   */
  it('1. POSITIVE: should list ALL theatres on /theatres/all GET', function (done) {
    chai.request(server)
      .get('/theatres/all')
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      })
  });

  /**
   * test GET a single theatre
   */
  it('2. POSITIVE: should list a SINGLE theatre on /theatres/theatre_id GET', function (done) {
    chai.request(server)
      .get('/theatres/' + testTheatreId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        res.body.data._id.should.equal(testTheatreId);
        done();
      });
  });

  it('3. NEGATIVE: should fail to list a non-existing theatre on /theatres/theatre_id GET', function (done) {
    chai.request(server)
      .get('/theatres/' + theatre_doesnot_exist.theatre_id)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  /**
   * test creating a new theatre
   */
  it('4. POSITIVE: should add a SINGLE theatre on /theatres/add POST', function (done) {
    chai.request(server)
      .post('/theatres/add')
      .send(newTheatre)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.theatreId.should.not.be.empty;
        res.body.data.theatreId.should.equal("T00001");
        done();
      });
  });

  /**
   * test updating a single theatre 
   */
  it('5. POSITIVE: should update a SINGLE theatre on /theatres/update POST', function (done) {
    chai.request(server)
      .post('/theatres/update')
      .send(updateTheatre)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.theatreId.should.not.be.empty;
        res.body.data.theatreId.should.equal(updateTheatre.theatreId);
        done();
      });
  });

  /**
   * test de-activating a single theatre 
   */
  it('6. POSITIVE: should delete a SINGLE theatre on /theatres/delete POST', function (done) {
    chai.request(server)
      .post('/theatres/delete')
      .send(theatre_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.theatreId.should.not.be.empty;
        res.body.data.theatreId.should.equal(theatre_exists.theatre_id);
        done();
      });
  });


  it('7. NEGATIVE: should fail to delete a non-existing theatre on /theatres/delete POST', function (done) {
    chai.request(server)
      .post('/theatres/delete')
      .send(theatre_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});

var testTheatreId = "T00000";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  dropCollection();
  var theatre = new theatreModel({
    "_id": testTheatreId,
    "theatreName": "Sangam Sarat",
    "status": "Active",
    "supervisors": [],
    "contact": {
      "landlineNumber": "08918736873",
      "mobileNumbers": [
        "9866411449",
        "9849354666"
      ]
    },
    "address": {
      "doorNumber": "68787",
      "street1": "jkghuigh",
      "street2": "jkllm",
      "landmark": "jkjkjkj",
      "city": "Anakapalle",
      "state": "Andhra Pradesh",
      "country": "India",
      "zipcode": "jblkgjnkl"
    },
    "screens": [
      { "name": "Sangam", "showTime": ["10:00", "13:00", "16:00", "21:00"] },
      { "name": "Sarat", "showTime": ["10:15", "13:15", "16:15", "21:15"] }
    ]
  });
  theatre.save(function (err) {
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollection();
  done();
}

function dropCollection() {
  theatreModel.collection.drop();
  counterModel.collection.drop();
}

var updateTheatre = {
  "theatreId": testTheatreId,
  "theatreName": "Sangam Sarat",
  "status": "Active",
  "supervisors": [],
  "contact": {
    "landlineNumber": "08918736873",
    "mobileNumbers": [
      "9866411449",
      "9849354666"
    ]
  },
  "address": {
    "doorNumber": "68787",
    "street1": "jkghuigh",
    "street2": "jkllm",
    "landmark": "jkjkjkj",
    "city": "Anakapalle",
    "state": "Andhra Pradesh",
    "country": "India",
    "zipcode": "jblkgjnkl"
  },
  "screens": [
    { "name": "Sangam", "showTime": ["10:00", "13:00", "16:00", "21:00"] },
    { "name": "Sarat", "showTime": ["10:15", "13:15", "16:15", "21:15"] }
  ]
};


var newTheatre = {
  "theatreName": "Sangam Sarat",
  "status": "Active",
  "supervisors": [],
  "contact": {
    "landlineNumber": "08918736873",
    "mobileNumbers": [
      "9866411449",
      "9849354666"
    ]
  },
  "address": {
    "doorNumber": "68787",
    "street1": "jkghuigh",
    "street2": "jkllm",
    "landmark": "jkjkjkj",
    "city": "Anakapalle",
    "state": "Andhra Pradesh",
    "country": "India",
    "zipcode": "jblkgjnkl"
  },
  "screens": [
    { "name": "Sangam", "showTime": ["10:00", "13:00", "16:00", "21:00"] },
    { "name": "Sarat", "showTime": ["10:15", "13:15", "16:15", "21:15"] }
  ]
};

var theatre_exists = { "theatre_id": testTheatreId };
var theatre_doesnot_exist = { "theatre_id": "abc1234" };
