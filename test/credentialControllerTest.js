process.env.NODE_ENV = 'test';

var credentialModel = require("../models/credential");
var credentialController = require("../routes/credentialController");
var credentialUtil = require("../utils/credentialUtil");

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var assert = chai.assert;
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "BEGIN CREDENTIAL CONTROLLER TESTS");
/**
 * Unit Test Cases
 */
describe('****CREDENTIAL CONTROLLER TESTS****', function () {

  /**
   * before executing test cases, drop collection.
   */
  credentialModel.collection.drop();
  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);


  it('1. POSITIVE: GET a credentialRecord from DB with employeeId alone', function (done) {
    chai.request(server)
      .get('/credentials/employeeid/' + employeeDetail_exists.employeeId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('2. POSITIVE: GET a credentialRecord from DB with userName alone', function (done) {
    chai.request(server)
      .get('/credentials/username/' + employeeDetail_exists.userName)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('3. NEGATIVE: fail to GET a credentialRecord that does not exist', function (done) {
    chai.request(server)
      .get('/credentials/employeeid/' + employeeDetail_does_not_exist.employeeId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
  it('4. NEGATIVE: fail to GET a credentialRecord that does not exist', function (done) {
    chai.request(server)
      .get('/credentials/username/' + employeeDetail_does_not_exist.userName)
      .end(function (err, res) {
         console.log(res.body);
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
  it('5. POSITIVE: UPDATE password of a credentialRecord in DB with both, employeeId and userName', function (done) {
    chai.request(server)
      .post('/credentials/update')
      .send(updateSuccessCredentialRecord)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('6. POSITIVE: UPDATE password of a credentialRecord in DB with employeeId alone', function (done) {
    chai.request(server)
      .post('/credentials/update')
      .send({ "employeeId": updateSuccessCredentialRecord.employeeId, "password": updateSuccessCredentialRecord.password })
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('7. POSITIVE: UPDATE password of a credentialRecord in DB with userName alone', function (done) {
    chai.request(server)
      .post('/credentials/update')
      .send({ "userName": updateSuccessCredentialRecord.userName, "password": updateSuccessCredentialRecord.password })
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('8. NEGATIVE: fail to UPDATE a credentialRecord that does not exist', function (done) {
    chai.request(server)
      .post('/credentials/update')
      .send(updateFailCredentialRecord)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
  it('9. NEGATIVE: fail to UPDATE a credentialRecord without userName and employeeId - {}', function (done) {
    chai.request(server)
      .post('/credentials/update')
      .send({})
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
  it('10. POSITIVE: DELETE a credentialRecord in DB with both, employeeId and userName', function (done) {
    chai.request(server)
      .post('/credentials/delete')
      .send(employeeDetail_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('11. POSITIVE: DELETE a credentialRecord in DB with employeeId alone', function (done) {
   chai.request(server)
      .post('/credentials/delete')
      .send({ "employeeId": employeeDetail_exists.employeeId})
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('12. POSITIVE: DELETE a credentialRecord in DB with userName alone', function (done) {
   chai.request(server)
      .post('/credentials/delete')
      .send({ "userName": employeeDetail_exists.userName})
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });
  it('13. NEGATIVE: fail to DELETE a credentialRecord that does not exist', function (done) {
    chai.request(server)
      .post('/credentials/delete')
      .send(employeeDetail_does_not_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
  it('14. NEGATIVE: fail to DELETE a credentialRecord without userName and employeeId - {}', function (done) {
   chai.request(server)
      .post('/credentials/delete')
      .send({})
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});

var testEmployeeId = "E00001";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  credentialModel.collection.drop();
  var credentialRecord = credentialModel(newCredentialRecord);
  credentialRecord.save(function (err) {
    if (err) {
      console.log("Error in beforeEachTest" + err);
    }
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  credentialModel.collection.drop();
  done();
}

/**
 * TEST DATA STUB
 */

var employeeId_exists = "E00000";
var userName_exists = "ABC_XYZ";

var employeeId_does_not_exist = "E00010";
var userName_does_not_exist = "ABC_XYZ_10";

var newCredentialRecord = {
  "_id": employeeId_exists,
  "userName": userName_exists,
  "password": credentialUtil.generatePassword("E00000"),
  "role": "supervisor",
  "status": "Active"
};

var updateSuccessCredentialRecord = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
  "password": "mysecretphrase",
  "role": "supervisor"
};

var updateFailCredentialRecord = {
  "_id": employeeDetail_does_not_exist,
  "userName": userName_does_not_exist,
  "password": "mysecretphrase",
  "role": "supervisor",
  "status": "Active"
};

var successCredentialRecord = {
  "_id": "E00001",
  "userName": "ABC_XYZ_1",
  "password": credentialUtil.generatePassword("E00001"),
  "role": "supervisor"
};

var failCredentialRecord = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
  "password": credentialUtil.generatePassword("E00001"),
  "role": "supervisor"
};

var employeeDetail_exists = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
}

var employeeDetail_does_not_exist = {
  "employeeId": employeeId_does_not_exist,
  "userName": userName_does_not_exist,
}
