process.env.NODE_ENV = 'test';

var SuccessResp = require('../models/out/successResp');
var FailureResp = require('../models/out/failureResp');
var winston = require('winston');

winston.log("info", "TEST OUT MESSAGES")
describe('****TEST OUT MESSAGES****', function () {
    it("1. POSITIVE: test success response", function (done) {
        var success = new SuccessResp({"name": "this is a successful response"});
        winston.log("info",JSON.stringify(success));
        done();
    });

    it("2. POSITIVE: test failure response", function (done) {
        var failure = new FailureResp("data retrieval failed","No Document in DB");
        winston.log("info", JSON.stringify(failure));
        done();
    });
});