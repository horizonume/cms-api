process.env.NODE_ENV = 'test';

var theatreMovieModel = require("../models/theatreMovie");
var counterModel = require("../models/counter");
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "Logging Theatre-Movie Test CRUD");
/**
 * Unit Test Cases
 */
describe('****THEATRE-MOVIE CRUD TESTS****', function () {
  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test GET all theatres
   */
  it('1. POSITIVE: should list ALL theatres on /theatresMovies/all GET', function (done) {
    chai.request(server)
      .get('/theatreMovies/all')
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      })
  });

  /**
   * test GET a movie-theatre associations of a theatre
   */
  it('2. POSITIVE: should list a SINGLE theatre on /theatreMovies/theatre/:theatre_id GET', function (done) {
    chai.request(server)
      .get('/theatreMovies/theatre/' + testTheatreId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      });
  });

  /**
   * test GET a movie-theatre associations of a movie
   */
  it('3. POSITIVE: should list a SINGLE theatre on /theatreMovies/movie/:movie_id GET', function (done) {
    chai.request(server)
      .get('/theatreMovies/movie/' + testMovieId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      });
  });


  it('4. NEGATIVE: should fail to list a non-existing theatre on /theatreMovies/theatre/:theatre_id GET', function (done) {
    chai.request(server)
      .get('/theatreMovies/theatre/' + movie_theatre_doesnot_exist.theatreId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  /**
   * test creating a new theatre
   */
  it('5. POSITIVE: should add a SINGLE theatre on /theatreMovies/add POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/add')
      .send(newTheatreMovie)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });

  /**
   * test updating a single theatre 
   */
  it('6. POSITIVE: should update a SINGLE theatre on /theatreMovies/update POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/update')
      .send(updateTheatreMovie)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });

  /**
   * test removing all movie-theatre association of a single theatre 
   */
  it('7. POSITIVE: should delete a  movie-theatre association for a movieId on /theatreMovies/delete/movie POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/movie')
      .send(movie_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });


  it('8. NEGATIVE: should fail to delete a non-existing theatre on /theatreMovies/delete/movie POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/movie')
      .send(movie_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);

        done();
      });
  });
  /**
   * test removing all movie-theatre association of a single theatre 
   */
  it('9. POSITIVE: should delete a movie-theatre on /theatreMovies/delete/theatreMovie POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/theatreMovie')
      .send(movie_theatre_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      });
  });


  it('10. NEGATIVE: should fail to delete a non-existing theatre on /theatreMovies/delete/theatreMovie POST', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/theatreMovie')
      .send(movie_theatre_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  it('11. NEGATIVE: should fail to delete a existing movie-theatre on /theatreMovies/delete/theatreMovie POST with movieId alone', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/theatreMovie')
      .send(movie_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  it('12. NEGATIVE: should fail to delete a existing movie-theatre on /theatreMovies/delete/theatreMovie POST with theatreId alone', function (done) {
    chai.request(server)
      .post('/theatreMovies/delete/theatreMovie')
      .send(theatre_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});




var testMovieId = "M00000";
var testTheatreId = "T00000";


/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  dropCollecction();
  var theatreMovie = new theatreMovieModel({
    "movieId": testMovieId,
    "movieName": "abcd",
    "theatreId": testTheatreId,
    "theatreName": "sangam_sarat",
    "screenName": "sangam",
    "showTime": "10:00",
    "fromDate": new Date(),
    "toDate": new Date()
  });
  theatreMovie.save(function (err) {
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollecction();
  done();
}

function dropCollecction(){
  theatreMovieModel.collection.drop();
}
var updateTheatreMovie = {
  "movieId": testMovieId,
  "movieName": "abcd_1234",
  "theatreId": testTheatreId,
  "theatreName": "sangam_sarat",
  "screenName": "sangam",
  "showTime": "10:00",
  "fromDate": new Date(),
  "toDate": new Date()
};

var newTheatreMovie = {
  "movieId": "M00001",
  "movieName": "abcd",
  "theatreId": "T00001",
  "theatreName": "sangam_sarat",
  "screenName": "sangam",
  "showTime": "10:00",
  "fromDate": new Date(),
  "toDate": new Date()
};

var movie_exists = { "movieId": testMovieId };
var movie_doesnot_exist = { "movieId": "abc1234" };
var theatre_exists = { "theatreId": testTheatreId };
var movie_theatre_exists = { "theatreId": testTheatreId, "movieId": testMovieId };
var movie_theatre_doesnot_exist = { "theatreId": "abc1234", "movieId": "abc1234" };
