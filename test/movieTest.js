process.env.NODE_ENV = 'test';

var movieModel = require("../models/movie");
var counterModel = require("../models/counter");
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "BEGIN MOVIE CRUD TESTS");
/**
 * Unit Test Cases
 */
describe('****MOVIE CRUD TESTS****', function () {

  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test GET all movies
   */
  it('1. POSITIVE: should list ALL movies on /movies/all GET', function (done) {
    chai.request(server)
      .get('/movies/all')
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        done();
      })
  });

  /**
   * test GET a single movie
   */
  it('2. POSITIVE: should list a SINGLE movie on /movies/movie_id GET', function (done) {
    chai.request(server)
      .get('/movies/' + testMovieId)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.should.not.be.empty;
        res.body.data._id.should.equal(testMovieId);
        done();
      });
  });

  it('3. NEGATIVE: should fail to list a non-existing movie on /movies/movie_id GET', function (done) {
    chai.request(server)
      .get('/movies/' + movie_doesnot_exist.movie_id)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });

  /**
   * test creating a new movie
   */
  it('4. POSITIVE: should add a SINGLE movie on /movies/add POST', function (done) {
    chai.request(server)
      .post('/movies/add')
      .send(newMovie)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.movieId.should.not.be.empty;
        res.body.data.movieId.should.equal("M00001");
        done();
      });
  });

  /**
   * test updating a single movie 
   */
  it('5. POSITIVE: should update a SINGLE movie on /movies/update POST', function (done) {
    chai.request(server)
      .post('/movies/update')
      .send(updateMovie)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.movieId.should.equal(updateMovie.movieId);
        done();
      });
  });

  /**
   * test de-activating a single movie 
   */
  it('6. POSITIVE: should delete a SINGLE movie on /movies/delete POST', function (done) {
    chai.request(server)
      .post('/movies/delete')
      .send(movie_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        res.body.data.movieId.should.equal(movie_exists.movie_id);
        done();
      });
  });


  it('7. NEGATIVE: should fail to delete a non-existing movie on /movies/delete POST', function (done) {
    chai.request(server)
      .post('/movies/delete')
      .send(movie_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      });
  });
});

var testMovieId = "M00000";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  dropCollection();
  var movie = new movieModel({
    "_id": testMovieId,
    "movieName": "SOSOSOSOSO",
    "cast": {
      "heroes": ["ABC", "XYZ"],
      "heroines": ["MNO", "PQR"]
    },

    "genre": "comedy",
    "banner": "Anjana Productions",
    "ratings": [
      { "reviewer": "Great Andhra", "rating": 3 },
      { "reviewer": "Idlebrain", "rating": 3.25 }
    ],
    "status": "Active"
  });
  movie.save(function (err) {
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollection();
  done();
}

function dropCollection() {
  movieModel.collection.drop();
  counterModel.collection.drop();
}

var updateMovie = {
  "movieId": testMovieId,
  "movieName": "POPOPOPOPO",
  "cast": {
    "heroes": ["ABC1", "XYZ1"],
    "heroines": ["MNO1", "PQR1"]
  },

  "genre": "comedy",
  "banner": "Anjani Productions",
  "ratings": [
    { "reviewer": "Great Andhra", "rating": 3.25 },
    { "reviewer": "Idlebrain", "rating": 3.75 }
  ],
  "status": "Active"
};


var newMovie = {
  "movieName": "POPOPOPOPO",
  "cast": {
    "heroes": ["ABC1", "XYZ1"],
    "heroines": ["MNO1", "PQR1"]
  },

  "genre": "comedy",
  "banner": "Anjani Productions",
  "ratings": [
    { "reviewer": "Great Andhra", "rating": 3.25 },
    { "reviewer": "Idlebrain", "rating": 3.75 }
  ]
};


var movie_exists = { "movie_id": testMovieId };
var movie_doesnot_exist = { "movie_id": "abc1234" };