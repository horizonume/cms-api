process.env.NODE_ENV = 'test';

var credentialModel = require("../models/credential");
var counterModel = require("../models/counter");
var employeeModel = require("../models/employee");

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();
var winston = require('winston');
chai.use(chaiHttp);

winston.log("info", "BEGIN LOGIN TESTS");
/**
 * Unit Test Cases
 */
describe('****LOGIN TESTS****', function () {


  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Test Authenticate an existing employee
   */
  it('1. POSITIVE: Authenticate user /login/authenticate GET', function (done) {
    chai.request(server)
      .post('/login/authenticate')
      .send(credential_exists)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(false);
        done();
      })
  });
  /**
   * Test Authenticate a non-existing employee
   */
  it('2. NEGATIVE: Fail to Authenticate a non-existing user /login/authenticate GET', function (done) {
    chai.request(server)
      .post('/login/authenticate')
      .send(credential_doesnot_exist)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      })
  });
  /**
   * Test Authenticate a non-existing employee
   */
  it('3. NEGATIVE: Fail to Authenticate user with wrong password /login/authenticate GET', function (done) {
    chai.request(server)
      .post('/login/authenticate')
      .send(wrong_password)
      .end(function (err, res) {
        res.should.have.status(200);
        res.body.error.should.equal(true);
        done();
      })
  });
});


/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  //before executing test cases, drop collection.
  dropCollection();
  chai.request(server)
    .post('/employees/add')
    .send(newEmployee)
    .end(function (err, res) {
      res.should.have.status(200);
      res.body.error.should.equal(false);
      credential_exists.password = res.body.data.password;
      done();
    });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  dropCollection();
  done();
}

function dropCollection() {
  employeeModel.collection.drop();
  counterModel.collection.drop();
  credentialModel.collection.drop();
}

/**
 * TEST DATA STUB
 */
var newEmployee = {
  "companyName": "tcs",
  "firstName": "ABC",
  "lastName": "XYZ",
  "userName": "ABC_XYZ",
  "designation": "supervisor",
  "role": "supervisor",
  "homeAddress_houseNumber": "502",
  "homeAddress_street1": "Sai Kota Serene,Vaishali Enclave",
  "homeAddress_street2": "Madinaguda",
  "homeAddress_landmark": "besise Swagath Grand",
  "homeAddress_city": "Hyderabad",
  "homeAddress_state": "Telangana",
  "homeAddress_country": "India",
  "homeAddress_zipcode": "500050",
  "mobileNumber": "1234567890",
  "landlineNumber": "0891-581024",
  "status": "Active"
};


var credential_exists = { "userName": newEmployee.userName, "password": "nmet00001" };
var credential_doesnot_exist = { "userName": newEmployee.userName + "_DNTEXT", "password": "nmet00001" };
var wrong_password = { "userName": newEmployee.userName, "password": "nmetabc" };