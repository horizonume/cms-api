process.env.NODE_ENV = 'test';

var credentialModel = require("../models/credential");
var credentialUtil = require("../utils/credentialUtil");

var chai = require('chai');
var server = require('../app');
var should = chai.should();
var assert = chai.assert;
var winston = require('winston');

winston.log("info", "BEGIN credentialUtil TESTS");
/**
 * Unit Test Cases
 */
describe('****CREDENTIAL UTIL TESTS****', function () {

  /**
   * before executing test cases, drop collection.
   */
  credentialModel.collection.drop();
  /**
   * run before each test case 
   */
  beforeEach(beforeEachTest);

  /**
   * run after each test case
   */
  afterEach(afterEachTest);

  /**
   * Insert a new credentialRecord into DB
   */
  it('1. POSITIVE: INSERT a new credentialRecord into DB', function (done) {
    var resultPromise = credentialUtil.create(successCredentialRecord);
    resultPromise.then(function (credentialRecord) {
      credentialRecord.should.not.be.null;
      done();
    }).catch(function (err) {
      should.equal(err, null);
      done();
    });
  });
  /**
   * Fail to insert a DUPLICATE user
   */
  it('2. NEGATIVE: fail to INSERT a DUPLICATE user ', function (done) {
    var resultPromise = credentialUtil.create(failCredentialRecord);
    resultPromise.then(function (credentialRecord, numAffected, err) {
      credentialRecord.should.be.null;
      numAffected.should.be.equal(0);
      err.should.not.be.null;
      done();
    }).catch(function (err) {
      err.should.not.be.null; // duplicate key error from DB
      done();
    });
  });
  it('3. POSITIVE: GET a credentialRecord from DB with both, employeeId and userName', function (done) {
    var resultPromise = credentialUtil.get(employeeDetail_exist);
    resultPromise.then(function (credentialRecord) {
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(employeeDetail_exist.employeeId);
      credentialRecord.userName.should.equal(employeeDetail_exist.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('4. POSITIVE: GET a credentialRecord from DB with employeeId alone', function (done) {
    var resultPromise = credentialUtil.get({ "employeeId": employeeDetail_exist.employeeId });
    resultPromise.then(function (credentialRecord) {
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(employeeDetail_exist.employeeId);
      credentialRecord.userName.should.equal(employeeDetail_exist.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('5. POSITIVE: GET a credentialRecord from DB with userName alone', function (done) {
    var resultPromise = credentialUtil.get({ "userName": employeeDetail_exist.userName });
    resultPromise.then(function (credentialRecord) {
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(employeeDetail_exist.employeeId);
      credentialRecord.userName.should.equal(employeeDetail_exist.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('6. NEGATIVE: fail to GET a credentialRecord that does not exist', function (done) {
    var resultPromise = credentialUtil.get(employeeDetail_does_not_exist);
    resultPromise.then(function (credentialRecord) {
      should.equal(credentialRecord, null);
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('7. NEGATIVE: fail to GET a credentialRecord without userName and employeeId - {}', function (done) {
    try {
      var resultPromise = credentialUtil.get({});
      resultPromise.then(function (credentialRecord, err) {
        should.equal(credentialRecord, null);
        done();
      }).catch(function (err) {
        should.equal(err, null);
        done();
      });
    } catch (err) { // expecting this block to get triggered
      err.should.not.be.null;
      done();
    }
  });
  it('8. POSITIVE: UPDATE password of a credentialRecord in DB with both, employeeId and userName', function (done) {
    var resultPromise = credentialUtil.update(updateSuccessCredentialRecord);
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(updateSuccessCredentialRecord.employeeId);
      credentialRecord.userName.should.equal(updateSuccessCredentialRecord.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      console.log("err = " + err);
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('9. POSITIVE: UPDATE password of a credentialRecord in DB with employeeId alone', function (done) {
    var resultPromise = credentialUtil.update({ "employeeId": updateSuccessCredentialRecord.employeeId, "password": updateSuccessCredentialRecord.password});
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(updateSuccessCredentialRecord.employeeId);
      credentialRecord.userName.should.equal(updateSuccessCredentialRecord.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('10. POSITIVE: UPDATE password of a credentialRecord in DB with userName alone', function (done) {
    var resultPromise = credentialUtil.update({ "userName": updateSuccessCredentialRecord.userName, "password": updateSuccessCredentialRecord.password});
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(updateSuccessCredentialRecord.employeeId);
      credentialRecord.userName.should.equal(updateSuccessCredentialRecord.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('11. NEGATIVE: fail to UPDATE a credentialRecord that does not exist', function (done) {
    var resultPromise = credentialUtil.update(updateFailCredentialRecord);
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      should.equal(credentialRecord, null);
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('12. NEGATIVE: fail to UPDATE a credentialRecord without userName and employeeId - {}', function (done) {
    try {
      var resultPromise = credentialUtil.update({});
      resultPromise.then(function (credentialRecord) {
        should.equal(credentialRecord, null);
        done();
      }).catch(function (err) {
        should.equal(err, null);
        done();
      });
    } catch (err) { // expecting this block to get triggered
      err.should.not.be.null;
      done();
    }
  });
  it('13. POSITIVE: DELETE a credentialRecord in DB with both, employeeId and userName', function (done) {
    var resultPromise = credentialUtil.delete(employeeDetail_exist);
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(updateSuccessCredentialRecord.employeeId);
      credentialRecord.userName.should.equal(updateSuccessCredentialRecord.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('14. POSITIVE: DELETE a credentialRecord in DB with employeeId alone', function (done) {
    var resultPromise = credentialUtil.delete({ "employeeId": employeeDetail_exist.employeeId});
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(employeeDetail_exist.employeeId);
      credentialRecord.userName.should.equal(employeeDetail_exist.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('15. POSITIVE: DELETE a credentialRecord in DB with userName alone', function (done) {
    var resultPromise = credentialUtil.delete({ "userName": employeeDetail_exist.userName});
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      credentialRecord.should.not.be.null;
      credentialRecord._id.should.equal(employeeDetail_exist.employeeId);
      credentialRecord.userName.should.equal(employeeDetail_exist.userName);
      credentialRecord.status.should.equal("Active");
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('16. NEGATIVE: fail to DELETE a credentialRecord that does not exist', function (done) {
    var resultPromise = credentialUtil.delete(employeeDetail_does_not_exist);
    resultPromise.then(function (credentialRecord) { // expecting this block to get triggered
      should.equal(credentialRecord, null);
      done();
    }).catch(function (err) {
      should.equal(err, null);
      assert.fail(err);
      done();
    });
  });
  it('17. NEGATIVE: fail to DELETE a credentialRecord without userName and employeeId - {}', function (done) {
    try {
      var resultPromise = credentialUtil.delete({});
      resultPromise.then(function (credentialRecord) {
        should.equal(credentialRecord, null);
        done();
      }).catch(function (err) {
        should.equal(err, null);
        done();
      });
    } catch (err) { // expecting this block to get triggered
      err.should.not.be.null;
      done();
    }
  });
});

var testEmployeeId = "E00001";

/**
 * function to insert a test document before each testcase
 */
function beforeEachTest(done) {
  credentialModel.collection.drop();
  var credentialRecord = credentialModel(newCredentialRecord);
  credentialRecord.save(function (err) {
    if (err) {
      console.log("Error in beforeEachTest" + err);
    }
    done();
  });
}

/**
 * function to drop collection after each test case
 */
function afterEachTest(done) {
  credentialModel.collection.drop();
  done();
}

/**
 * TEST DATA STUB
 */

var employeeId_exists = "E00000";
var userName_exists = "ABC_XYZ";

var employeeId_does_not_exist = "E00010";
var userName_does_not_exist = "ABC_XYZ_10";

var newCredentialRecord = {
  "_id": employeeId_exists,
  "userName": userName_exists,
  "password": credentialUtil.generatePassword("E00000"),
  "role": "supervisor",
  "status": "Active"
};

var updateSuccessCredentialRecord = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
  "password": "mysecretphrase",
  "role": "supervisor"
};

var updateFailCredentialRecord = {
  "_id": employeeDetail_does_not_exist,
  "userName": userName_does_not_exist,
  "password": "mysecretphrase",
  "role": "supervisor",
  "status": "Active"
};

var successCredentialRecord = {
  "_id": "E00001",
  "userName": "ABC_XYZ_1",
  "password": credentialUtil.generatePassword("E00001"),
  "role": "supervisor"
};

var failCredentialRecord = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
  "password": credentialUtil.generatePassword("E00001"),
  "role": "supervisor"
};

var employeeDetail_exist = {
  "employeeId": employeeId_exists,
  "userName": userName_exists,
}

var employeeDetail_does_not_exist = {
  "employeeId": employeeId_does_not_exist,
  "userName": userName_does_not_exist,
}
