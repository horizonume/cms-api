var CredentialModel = require("../models/credential");
var winston = require('winston');

var credentialUtil = {};
credentialUtil.create = function (credentialRecord) {
        var credentialModel = new CredentialModel(credentialRecord);
        var savePromise = credentialModel.save();
        return savePromise;
}

credentialUtil.get = function(credentialRecord) {
    var findQuery = parseCredential(credentialRecord);
    if (findQuery) {
        var findPromise = CredentialModel.findOne(findQuery);
        return findPromise;
    } else {
        throw "both, employeeId and userName are null";
    }
}

credentialUtil.update = function(credentialRecord) {
    var updateQuery = parseCredential(credentialRecord);
    if (updateQuery) {
        var updatePromise = CredentialModel.findOneAndUpdate(updateQuery, { "password": credentialRecord.password });
        return updatePromise;
    } else {
        throw "both, employeeId and userName are null";
    }
}

credentialUtil.delete = function(credentialRecord) {
    var deleteQuery = parseCredential(credentialRecord);
    if (deleteQuery) {
        var deletePromise = CredentialModel.findOneAndUpdate(deleteQuery, { "status": "Deleted" });
        return deletePromise;
    } else {
        throw "both, employeeId and userName are null";
    }
}

credentialUtil.generatePassword = function(passSuffix) {
    return "nmet" + passSuffix.substring(1, 6);
}

function parseCredential(credentialRecord) {
    if (!credentialRecord.employeeId && !credentialRecord.userName) {
        return null;
    }
    var record = { "status": "Active" };
    if (credentialRecord.employeeId)
        record._id = credentialRecord.employeeId;
    if (credentialRecord.userName)
        record.userName = credentialRecord.userName;
    return record;
}

module.exports = credentialUtil;