var counterModel = require("../models/counter");
/**
 * @name - name of the the counter, example : employee, item etc..
 * utility that returns a query that when executed returns next sequence for a given counter.
 */
var mongoseq
 = function getNextSequence(name) {
  return counterModel.findOneAndUpdate({ _id: name }, { $inc: { seq: 1 } }, { new: true, upsert: true });
}

module.exports = mongoseq;
