var express = require('express');
var router = express.Router();
var syncModel = require("../models/testSyncModel");

router.post('/', function (req, res, next) {

    var response = {};
    syncModel.find({ "phoneNumber": req.body.phoneNumber }, function (err, data) {
        setTimeout(console.log("inside find method, executing after 2 seconds"), 2000);

        if (err) {
            response = { "error": true, "message": "Error Inserting Record" };
        } else {
            if (data) {
                response = { "error": true, "message": "Given Phone Number is already registered" };
            } else {
                response = { "error": false, "message": "Phone Number not in use" };
            }
        }
    });
    console.log("Executing sysout",response);
    res.json(response); 
});

module.exports = router;