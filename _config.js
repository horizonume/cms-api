var config = {};

/**
 * mongo connection URL
 */
config.mongoURI = {
  development: 'mongodb://localhost:27017/cms',
  test: 'mongodb://localhost:27017/cmsTest',
  prod: '<TBD>',
};



/**
 * GENERIC LOG CONFIG 
 */
config.log = {};
config.log.dir = {
  development: 'log',
  test: 'log',
  prod: 'log'
};

config.log.ext = {
  development: '.log',
  test: '.log',
  prod: '.log'
};

/**
 * HTTP LOGGER CONFIG
 */
config.log.http = {};
/**
 * morgan logformat for logging http requests
 * example: POST /employees/delete 200 64 - 2.150 ms 
 */
config.log.http.logformat = {
  development: 'dev',
  test: 'tiny',
  prod: 'common'
};

config.log.http.logname = {
  development: 'devAccess',
  test: 'testAccess',
  prod: 'access'
};

/**
 * CODE LOGGER CONFIG
 */
config.log.code = {};
config.log.code.logname = {
  development: 'dev',
  test: 'test',
  prod: '<TBD>'
};
module.exports = config;
