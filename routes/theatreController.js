var express = require('express');
var router = express.Router();
var theatreModel = require("../models/theatre");
var mongoseq = require("../utils/mongoseq");
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');

/**
 *  GET ALL THEATRES 
 */
router.get('/all', function (req, res, next) {

  var response = {};
  theatreModel.find({ status: "Active" }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error Fetching Theatres", err.errmsg);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * GET AN THEATRE WITH GIVEN ID ":theatre_id"
 */
router.get('/:theatre_id', function (req, res, next) {

  var response = {};
  theatreModel.findById({ "_id": req.params.theatre_id }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error Fetching Theatre", err.errmsg);
    } else {
      if (data) {
        response = new SuccessResponse(data);
      } else {
        response = new FailureResponse("Theatre  " + req.params.theatre_id + " does not exist");
      }
    }
    res.json(response);
  });
});

/**
 * update theatre
 */
router.post('/update', function (req, res, next) {
  var updateTheatre = {
    "_id": req.body.theatreId,
    "theatreName": req.body.theatreName,
    "screens": req.body.screens,
    "address": {
      "doorNumber": req.body.address_doorNumber,
      "street1": req.body.address_street1,
      "street2": req.body.address_street2,
      "landmark": req.body.address_landmark,
      "city": req.body.address_city,
      "state": req.body.address_state,
      "country": req.body.address_country,
      "zipcode": req.body.address_zipcode,
    },
    "contact": {
      "mobileNumber": req.body.mobileNumbers,
      "landlineNumber": req.body.landlineNumber
    },
    "supervisors": req.body.supervisors,
    "status": "Active"

  };
  var response = {};
  theatreModel.findOneAndUpdate({ _id: req.body.theatreId }, updateTheatre, function (err) {
    if (err) {
      response = new FailureResponse("Error Updating Theatre ", err.errmsg);
    }
    else {
      var message = "Theatre " + req.body.theatreId + " updated successfully";
      response = new SuccessResponse({ "theatreId": req.body.theatreId }, message);
    }
    res.json(response);
  });

});

/**
 * Create a new theatre
 */
router.post('/add', function (req, res, next) {
  var nextSeqQuery = mongoseq("theatreCounter");
  nextSeqQuery.exec(
    function (err, seqData) {
      if (err) {
        response = new FailureResponse("Error in Generating TheatreID ", err.errmsg);

      } else {
        // Pad the generated employeeid to make it a 5 digit number.
        var theatreId = "T" + ("00000" + seqData.seq).slice(-5);
        /* Creating New Employee Record */
        var newTheatre = new theatreModel({

          "_id": theatreId,
          "theatreName": req.body.theatreName,
          "screens": req.body.screens,
          "address": {
            "doorNumber": req.body.address_doorNumber,
            "street1": req.body.address_street1,
            "street2": req.body.address_street2,
            "landmark": req.body.address_landmark,
            "city": req.body.address_city,
            "state": req.body.address_state,
            "country": req.body.address_country,
            "zipcode": req.body.address_zipcode,
          },
          "contact": {
            "mobileNumbers": req.body.mobileNumbers,
            "landlineNumber": req.body.landlineNumber
          },
          "supervisors": req.body.supervisors,
          "status": "Active"

        });

        var response = {};
        newTheatre.save(function (err) {
          // Mongo command to fetch all data from collection.
          if (err) {
            console.log("error saving  " + err);
            if (err.code === 11000) {

              response = new FailureResponse("Error Adding Theatre ", "Mobile number is already registered");
            } else {
              response = new FailureResponse("Error Adding Theatre ", err.errmsg);

            }
          }
          else {
            var message = "Theatre " + theatreId + " added successfully";
            response = new SuccessResponse({ "theatreId": theatreId }, message);
          }
          res.json(response);
        });
      }
    }
  );

});

router.post('/delete', function (req, res, next) {

  var theatreId = req.body.theatre_id;
  theatreModel.findOneAndUpdate({ "_id": theatreId }, { "status": "Deleted" }, function (err, data) {
    var response = {};
    if (err) {
      response = new FailureResponse("Error Deleting Theatre ", err.errmsg);

    } else if (!data) {
      var reason = "Theatre " + theatreId + " does not exist";
      response = new FailureResponse("error deleting theatre", reason);
    } else {
      message = "Theatre " + theatreId + " deleted successfully";

      response = new SuccessResponse({ "theatreId": theatreId }, message);
    }
    res.json(response);
  });
});

module.exports = router;
