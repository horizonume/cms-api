var express = require('express');
var router = express.Router();
var employeeModel = require("../models/employee");
var credential = require("../utils/credentialUtil");
var mongoseq = require("../utils/mongoseq");
var empPrefix = "E";
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');
var winston = require('winston');


/**
 *  GET ALL EMPLOYEES 
 */
router.get('/all', function (req, res, next) {

  var response = {};
  employeeModel.find({ status: "Active" }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error Fetching Employees", err.errmsg);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * GET AN EMPLOYEE WITH GIVEN ID ":employee_id"
 */
router.get('/:employee_id', function (req, res, next) {

  var response = {};
  employeeModel.findById({ "_id": req.params.employee_id }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error fetching Employee: " + req.params.employee_id, err.errmsg);
    } else if (!data) {
      var failureMessage = "error fetching employee details"
      var failureResponse = "Employee" + req.params.employee_id + " does not exist";
      response = new FailureResponse(failureMessage, failureResponse);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * update employee
 */
router.post('/update', function (req, res, next) {
  var employeeId = req.body.employeeId;
  var updateEmployee = {
    "_id": employeeId,
    "companyName": req.body.companyName,
    "firstName": req.body.firstName,
    "lastName": req.body.lastName,
    "userName": req.body.userName,
    "designation": req.body.designation,
    "role": req.body.role,
    "homeAddress": {
      "houseNumber": req.body.homeAddress_houseNumber,
      "street1": req.body.homeAddress_street1,
      "street2": req.body.homeAddress_street2,
      "landmark": req.body.homeAddress_landmark,
      "city": req.body.homeAddress_city,
      "state": req.body.homeAddress_state,
      "country": req.body.homeAddress_country,
      "zipcode": req.body.homeAddress_zipcode,
    },
    "contact": {
      "mobileNumber": req.body.mobileNumber,
      "landlineNumber": req.body.landlineNumber
    },
    "status": "Active"
  };
  var response = {};
  employeeModel.findOneAndUpdate({ _id: employeeId, "status": "Active" }, updateEmployee, function (err, data) {
    if (err) {
      response = new FailureResponse("error updating employee", err.errmsg);
    }
    else if (!data) {
      var reason = "employee: " + employeeId + " does not exist";
      response = new FailureResponse("error adding employee", reason);
    }
    else {
      var successMessage = "employee updated successfully";
      var updated = new Employee(employeeId);
      response = new SuccessResponse(updated, successMessage);
    }
    res.json(response);
  });
});

/**
 * Create a new employee
 */
router.post('/add', function (req, res, next) {
  var nextSeqQuery = mongoseq("employeeCounter");
  nextSeqQuery.exec(
    function (err, seqData) {
      if (err) {
        response = new FailureResponse("Error in Generating EmployeeID");
      } else {
        // Pad the generated employeeid to make it a 5 digit number.
        var employeeId = empPrefix + ("00000" + seqData.seq).slice(-5);

        /* Creating New Employee Record */

        var newEmployeeRecord = {
          "_id": employeeId,
          "companyName": req.body.companyName,
          "firstName": req.body.firstName,
          "lastName": req.body.lastName,
          "userName": req.body.userName,
          "designation": req.body.designation,
          /* role should be part of credential collection. Discuss*/
          //"role": req.body.role,
          "homeAddress": {
            "houseNumber": req.body.homeAddress_houseNumber,
            "street1": req.body.homeAddress_street1,
            "street2": req.body.homeAddress_street2,
            "landmark": req.body.homeAddress_landmark,
            "city": req.body.homeAddress_city,
            "state": req.body.homeAddress_state,
            "country": req.body.homeAddress_country,
            "zipcode": req.body.homeAddress_zipcode,
          },
          "contact": {
            "mobileNumber": req.body.mobileNumber,
            "landlineNumber": req.body.landlineNumber
          },
          "status": "Active"
        };
        var newEmployee = new employeeModel(newEmployeeRecord);
        var response = {};
        newEmployee.save(function (err) {
          // Mongo command to fetch all data from collection.
          if (err) {
            if (err.code === 11000) {
              winston.log("error", err);
              response = new FailureResponse("error adding employee", "mobile number is already registered");
              res.json(response);
            } else {
              response = new FailureResponse("error adding employee", err.errmsg);
              res.json(response);
            }
          } else {
            var credentialRecord = {
              "_id": employeeId,
              "userName": req.body.userName,
              "password": credential.generatePassword(employeeId),
              "role": req.body.role
            };
            /**
             * if employee record created successfully, create login credentials
             */
            var createCredentialPromise = credential.create(credentialRecord);
            winston.log("DEBUG", "promise: " + JSON.stringify(createCredentialPromise));
            createCredentialPromise.then(function (credentialRecord) {
                var successMessage = "employee added successfully";
                var added = new Employee(employeeId);
                response = new SuccessResponse({ "password": credentialRecord.password }, successMessage);
                res.json(response);
            }).catch(function (err) {
              winston.log("error", "credential creation failed for :" + employeeId + "with err: " + err +  ",\n hence reverting employee creation");
              employeeModel.findByIdAndRemove({ "_id": employeeId }, function (err, data) {
                if (err) {
                  winston.log("error", "INCONSISTENT: revert employeerecord failed after credential creation failure" + employeeId);
                }
                response = new FailureResponse("error adding employee: ","could not create credentials for employee");
                res.json(response);
              });
            });
          }
        }
        );
      }
    })
});




router.post('/delete', function (req, res, next) {

  var employeeId = req.body.employee_id;
  employeeModel.findOneAndUpdate({ "_id": employeeId, "status": "Active" }, { "status": "Deleted" }, function (err, data) {
    var response = {};
    if (err) {
      failureMessage = "Error Deleting Employee : " + employeeId;
      response = new FailureResponse(failureMessage, err.errmsg);
    } else if (!data) {
      var reason = "employee: " + req.body.employeeId + " does not exist";
      response = new FailureResponse("error deleting employee", reason);
    }
    else {
      var deleted = new Employee(employeeId);
      successMessage = "Employee " + employeeId + " deleted successfully";
      response = new SuccessResponse(deleted, successMessage);
    }
    res.json(response);
  });
});


function Employee(employeeId) {
  this.employeeId = employeeId;
}
module.exports = router;

