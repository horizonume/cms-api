var express = require('express');
var router = express.Router();
var theatreItemModel = require("../models/theatreItem");
var mongoseq = require("../utils/mongoseq");
//var empPrefix = "E";
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');

/**
 *  GET ALL THEATRE ITEMS
 */
router.get('/all', function (req, res, next) {

  var response = {};
  theatreItemModel.find({ status: "Active" }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error Fetching Theatre Items", err.errmsg);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * GET AN ITEM WITH GIVEN ID ":th_item_code"
 */
router.get('/:th_item_code', function (req, res, next) {

  var response = {};
  theatreItemModel.findById({ "_id": req.params.th_item_code }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error fetching Item: " + req.params.th_item_code, err.errmsg);
    } else if (!data) {
      var failureMessage = "error fetching item details"
      var failureResponse = "Item" + req.params.th_item_code + " does not exist";
      response = new FailureResponse(failureMessage, failureResponse);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * GET AN THEATRE ID FETCH ALL ITEMS WITH GIVEN ID ":th_code"
 */
router.get('/theatre/:th_code', function (req, res, next) {

  var response = {};
  theatreItemModel.find({ "theatreId": req.params.th_code, "status": "Active" }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error fetching Items: " + req.params.th_item_code, err.errmsg);
    } else if (!data) {
      var failureMessage = "error fetching item details"
      var failureResponse = "No Items for theatre" + req.params.th_code + "  exist";
      response = new FailureResponse(failureMessage, failureResponse);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * update theatre item
 */
router.post('/update', function (req, res, next) {
  var theatreItemId = req.body.theatreItemId;
  var updateTheatreItem = {
    "_id" : req.body.theatreItemId,
    "theatreId" :  req.body.theatreId,
    "theatreName":  req.body.theatreName,
      "itemCode": req.body.itemCode, "itemName": req.body.itemName, "supplierName": req.body.supplierName,
    "supplierContactName": req.body.supplierContactName,
        "supplierContactNumber":req.body.supplierContactNumber,"quantity":req.body.quantity,"perishable":req.body.perishable,
    "status": "Active"

  };
  var response = {};
  theatreItemModel.findOneAndUpdate({ _id: req.body.theatreItemId, "status": "Active" }, updateTheatreItem, function (err, data) {
    if (err) {
      response = new FailureResponse("error updating Item", err.errmsg);
    }
    else if (!data) {
      var reason = "item: " + theatreItemId + " does not exist";
      response = new FailureResponse("error adding item", reason);
    }
    else {
      var successMessage = "Item updated successfully";
      var updated = new TheatreItem(theatreItemId);
      response = new SuccessResponse(updated, successMessage);
    }
    res.json(response);
  });
});

/**
 * Create a new theatre item
 */
router.post('/add', function (req, res, next) {

        var theatreItemId = req.body.theatreId+req.body.itemCode;

        /* Creating New Theatre Item Record */
        var theatreItem = new theatreItemModel({

            "_id" : theatreItemId,
    "theatreId" :  req.body.theatreId,
    "theatreName":  req.body.theatreName,
      "itemCode": req.body.itemCode, "itemName": req.body.itemName, "supplierName": req.body.supplierName,
    "supplierContactName": req.body.supplierContactName,
        "supplierContactNumber":req.body.supplierContactNumber,"quantity":req.body.quantity,"perishable":req.body.perishable,
    "status": "Active"


        });
        var response = {};
        theatreItem.save(function (err) {
          // Mongo command to fetch all data from collection.
          if (err) {
            if (err.code === 11000) {
              console.log(err);
              response = new FailureResponse("error adding item", "Item already added");
            } else {
              response = new FailureResponse("error adding item", err.errmsg);
            }
          }
          else {
            var successMessage = "Item added successfully";
            var added = new TheatreItem(theatreItemId);
            response = new SuccessResponse(added, successMessage);
          }
          res.json(response);
        });
      }
 
  );


router.post('/delete', function (req, res, next) {

  var theatreItemId = req.body.theatreItem_id;
  theatreItemModel.findOneAndUpdate({ "_id": theatreItemId, "status": "Active" }, { "status": "Deleted" }, function (err, data) {
    var response = {};
    if (err) {
      failureMessage = "Error Deleting Item : " + theatreItemId;
      response = new FailureResponse(failureMessage, err.errmsg);
    } else if (!data) {
      var reason = "Item : " + req.body.theatreItem_id + " does not exist";
      response = new FailureResponse("error deleting item", reason);
    }
    else {
      var deleted = new TheatreItem(theatreItemId);
      successMessage = "Item " + theatreItemId + " deleted successfully";
      response = new SuccessResponse(deleted, successMessage);
    }
    res.json(response);
  });
});


function TheatreItem(theatreItemId){
  this.theatreItemId = theatreItemId;
}
module.exports = router;

