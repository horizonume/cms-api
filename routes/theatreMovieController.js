var express = require('express');
var router = express.Router();
var theatreMovieModel = require("../models/theatreMovie");
var mongoseq = require("../utils/mongoseq");
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');
var winston = require('winston');


/**
 * TO DO:
 * WHILE INTEGRATING WITH FRONTEND TAKE CARE OF "fromDate" and "toDate" format IN
 * /add and /update methods 
 */


/**
 *  GET ALL THEATRES 
 */
router.get('/all', function (req, res, next) {
    var response = {};
    theatreMovieModel.find(function (err, data) {
        if (err) {
            response = new FailureResponse("Error Fetching Theatre-Movie Mapping", err.errmsg);
        } else {
            response = new SuccessResponse(data);
        }
        res.json(response);
    });
});

/**
 * GET ALL MOVIES PLAYING IN ALL SCREENS OF A THEATRE WITH GIVEN THEATRE_ID ":theatre_id"
 */
router.get('/theatre/:theatre_id', function (req, res, next) {
    var response = {};
    theatreMovieModel.find({ "theatreId": req.params.theatre_id }, function (err, data) {
        if (err) {
            response = new FailureResponse("Error Fetching Theatre-Movie Association", err.errmsg);
        } else if (data.length === 0) { // CAUTION: empty Array 
            response = new FailureResponse("No movies associated with theatre " + req.params.theatre_id + " does not exist");
        } else {
            response = new SuccessResponse(data);
        }
        res.json(response);
    });
});

/**
 * GET ALL MOVIES PLAYING IN ALL SCREENS OF A THEATRE WITH GIVEN THEATRE_ID ":theatre_id"
 */
router.get('/movie/:movie_id', function (req, res, next) {
    var response = {};
    theatreMovieModel.find({ "movieId": req.params.movie_id }, function (err, data) {
        if (err) {
            response = new FailureResponse("Error Fetching Theatre-Movie Association", err.errmsg);
        } else if (data.length === 0) { // CAUTION: empty Array 
            response = new FailureResponse("No theatres associated with the movie  " + req.params.movie_id + " does not exist");
        } else {
            response = new SuccessResponse(data);
        }
        res.json(response);
    });
});

/**
 * update theatre
 */
router.post('/update', function (req, res, next) {
    var updateTheatreMovie = {
        "movieId": req.body.movieId,
        "movieName": req.body.movieName,
        "theatreId": req.body.theatreId,
        "theatreName": req.body.theatreName,
        "screenName": req.body.screenName,
        "showTime": req.body.movieId,
        "fromDate": req.body.fromDate,
        "toDate": req.body.toDate
    };
    var response = {};
    theatreMovieModel.findOneAndUpdate({ theatreId: req.body.theatreId, movieId: req.body.movieId, screenName: req.body.screenName, showTime: req.body.showTime }, updateTheatreMovie, function (err) {
        if (err) {
            response = new FailureResponse("Error Updating Theatre-Movie Association ", err.errmsg);
        }
        else {
            var message = "Details updated successfully";
            response = new SuccessResponse({ "movieId": req.body.movieId, "theatreId": req.body.theatreId, "screenName": req.body.screenName }, message);
        }
        res.json(response);
    });

});

/**
 * Create a new MOVIE with a SCREEN of a THEATRE
 */
router.post('/add', function (req, res, next) {
    var newTheatreMovie = new theatreMovieModel({
        "movieId": req.body.movieId,
        "movieName": req.body.movieName,
        "theatreId": req.body.theatreId,
        "theatreName": req.body.theatreName,
        "screenName": req.body.screenName,
        "showTime": req.body.movieId,
        "fromDate": req.body.fromDate,
        "toDate": req.body.toDate
    });

    var response = {};
    newTheatreMovie.save(function (err) {
        // Mongo command to fetch all data from collection.
        if (err) {
            if (err.code === 11000) {
                response = new FailureResponse("Error Associating Movie with Screen of Theatre ", "Another MOVIE SCREENING ");
            } else {
                response = new FailureResponse("Error Associating Movie with Screen of Theatre ", err.errmsg);
            }
        }
        else {
            var message = "Movie added successfully";
            response = new SuccessResponse({}, message);
        }
        res.json(response);
    });
});

router.post('/delete/movie', function (req, res, next) {

    var movieId = req.body.movieId;
    var response = {};
    theatreMovieModel.remove({ "movieId": movieId }, function (err, data) {
        if (err) {
            response = new FailureResponse("error removing movie ", err.errmsg);
        } else if (data.result.n === 0 || data.result.ok !== 1) {
            var reason = "movie " + movieId + " does not exist";
            response = new FailureResponse("error deleting movie", reason);
        } else {
            message = "Movie " + movieId + " deleted successfully";
            response = new SuccessResponse({ "movieId": movieId }, message);
        }
        res.json(response);
    });
});

router.post('/delete/theatreMovie', function (req, res, next) {
    var response = {};
    var movieId = req.body.movieId;
    var theatreId = req.body.theatreId;
    if (!movieId) {
        response = new FailureResponse("error removing movie ", "movieId cannot be empty");
        res.json(response);
    } else if (!theatreId) {
        response = new FailureResponse("error removing movie ", "theatreId cannot be empty");
        res.json(response);
    } else {
        theatreMovieModel.remove({ "movieId": movieId, "theatreId": theatreId }, function (err, data) {
            if (err) {
                response = new FailureResponse("error removing movie ", err.errmsg);
            } else if (data.result.n === 0 || data.result.ok !== 1) {
                var reason = "movie does not exist";
                response = new FailureResponse("error deleting movie", reason);
            } else {
                message = "Delete successful";
                response = new SuccessResponse({ "movieId": movieId, "theatreId": theatreId }, message);
            }
            res.json(response);
        });
    }
});

module.exports = router;
