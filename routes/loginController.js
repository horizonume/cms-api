var express = require('express');
var router = express.Router();
var loginModel = require("../models/loginAudit");
var credentialModel = require("../models/credential");
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');
var winston = require('winston');

/**
 * 1. Handle login request
 *      authenticate user
 *      authorize user
 *      audit log both successful and unsuccessful logins
 * 2. interceptors
 */

router.post('/authenticate', function (req, res, next) {
    var userName = req.body.userName;
    var password = req.body.password;
    var response = {};
    credentialModel.findOne({ "userName": userName, "status": "Active" }, function (err, data) {
        if (err) {
            winston.log("INFO", "authentication failure for user : " + userName);
            response = new FailureResponse("authentication failure", err.errmsg);
        } else if (!data) {
            response = new FailureResponse("authentication failure", "user not registered");
        } else {
            if (password === data.password) {
                response = new SuccessResponse("Authentication Success");
            } else {
                response = new FailureResponse("authentication failure", "invalid username or password");
            }
        }
        res.json(response);
    });
});

module.exports = router;