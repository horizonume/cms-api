
/**
 * Not used in code
 * Used for testing mongoseq operation
 */
var express = require('express');
var router = express.Router();
var mongoseq = require("../utils/mongoseq");

router.get("/:counter_name", function (req, res, next) {
    var query = mongoseq(req.params.counter_name);
    console.log(query);
    query.exec(
        function (err, data) {
            if (err) {
                console.log(err);
                response = { "error": true, "message": "Error Incrementing Data" };
            } else {
                response = { "error": false, "data": data };
            }
            console.log(response);
            res.json(response.data.seq);
        }
    );


});


module.exports = router;