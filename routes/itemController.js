var express = require('express');
var router = express.Router();
var itemModel = require("../models/item");

/* GET items listing. */
router.get('/all', function (req, res, next) {

  var response = {};
  itemModel.find({ status: "Active" }, function (err, data) {
    // Mongo command to fetch all data from collection.
    if (err) {
      response = { "error": true, "message": "Error fetching data" };

    } else {
      response = { "error": false, "data": data };
    }
    res.json(response);
  });
});

/* GET item . */
router.get('/:item_id', function (req, res, next) {

  var response = {};
  itemModel.findById({ _id: req.params.item_id }, function (err, data) {
    // Mongo command to fetch all data from collection.
    if (err) {
      response = { "error": true, "message": "Error fetching data" };
    } else {
      response = { "error": false, "data": data };
    }
    res.json(response);
  });
});

/* add new item */
router.post('/add', function (req, res, next) {
  var newItem = new itemModel({
    "itemCode": req.body.itemCode,
    "itemName": req.body.itemName,
    "status": "Active"
  });
  var response = {};
  newItem.save(function (err) {
    // Mongo command to fetch all data from collection.
    if (err) {
      response = { "error": true, "message": "Error adding item" };
    }

    else {
      response = { "error": false, "message": "Item added successfully" };
    }
    res.json(response);
  });
});

/* update item */
router.post('/update', function (req, res, next) {

  var response = {};

  itemModel.findById({ itemCode: req.body.itemId }, function (err, item) {
    // Mongo command to fetch all data from collection.
    //  var item = items[0];
    item.itemName = req.body.itemName;
    item.itemCode = req.body.itemCode;

    item.save(function (err) {

      if (err) {
        response = { "error": true, "message": "Error fetching data" };
      } else {
        response = { "error": false, "message": "Item updated succesfully" };
      }
      res.json(response);
    });


  });
});

/* DELETE item . */
router.post('/delete', function (req, res, next) {

  var response = {};

  itemModel.findAndUpdate({ _id: req.body.itemId },{"status" :"Deleted"}, function (err, item) {
  
      var itemId = req.body.itemId;
      var itemName = req.body.itemName;
      if (err) {
        var failureMessage = "Error deleting item : " + itemName ; 
        response = { "error": true, "message": failureMessage };
      } else {
        var successMessage =  "Item: " + itemName + "Deleted succesfully";
        response = { "error": false, "message":  successMessage };
      }
      res.json(response);
    });
  
});
module.exports = router;
