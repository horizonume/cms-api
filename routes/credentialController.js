var express = require('express');
var router = express.Router();
var credentialModel = require("../models/credential");
var credentialUtil = require("../utils/credentialUtil");
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');
/**
 * 1. handle change password - DONE
 * 2. get credentials - DONE
 * 3. delete credentials - DONE
 * 
 * Note: change/update of userName and employeeId should not be supported.
 * Reason: Tracking a userName/employeeId change creates inconsistencies during employee 
 * login and loginAudit. 
 */

router.get('/employeeid/:employee_id', function (req, res, next) {
    var credentialRecord = {
        "employeeId": req.params.employee_id
    }
    var response = {};
    try {
        var resultPromise = credentialUtil.get(credentialRecord);
        resultPromise.then(function (resultRecord) {
            if (resultRecord) {
                response = new SuccessResponse(resultRecord);
                res.json(response);
            } else {
                response = new FailureResponse("credential not found", err);
                res.json(response);
            }
        }).catch(function (err) {
            response = new FailureResponse("credential not found", err);
            res.json(response);
        });
    } catch (err) {
        response = new FailureResponse("credential not found", err);
        res.json(response);
    }
});

router.get('/username/:user_name', function (req, res, next) {
    var credentialRecord = {
        "userName": req.params.user_name
    }
    var response = {};
    try {
        var resultPromise = credentialUtil.get(credentialRecord);
        resultPromise.then(function (resultRecord) {
            if (resultRecord) {
                response = new SuccessResponse(resultRecord);
                res.json(response);
            } else {
                response = new FailureResponse("credential not found", err);
                res.json(response);
            }
        }).catch(function (err) {
            response = new FailureResponse("credential not found", err);
            res.json(response);
        });
    } catch (err) {
        response = new FailureResponse("credential not found", err);
        res.json(response);
    }
});

router.post('/update', function (req, res, next) {
    var credentialRecord = {
        "employeeId": req.body.employeeId,
        "userName": req.body.userName,
        "password": req.body.password
    }
    var response = {};
    try {
        var resultPromise = credentialUtil.update(credentialRecord);
        resultPromise.then(function (resultRecord) {
            if (resultRecord) {
                response = new SuccessResponse(resultRecord);
                res.json(response);
            } else {
                response = new FailureResponse("credential not found", err);
                res.json(response);
            }
        }).catch(function (err) {
            response = new FailureResponse("password update failed", err);
            res.json(response);
        });
    } catch (err) {
        response = new FailureResponse("password update failed", err);
        res.json(response);
    }
});

router.post('/delete', function (req, res, next) {
    var credentialRecord = {
        "employeeId": req.body.employeeId,
        "userName": req.body.userName
    }
    var response = {};
    try {
        var resultPromise = credentialUtil.delete(credentialRecord);
        resultPromise.then(function (resultRecord) {
           if (resultRecord) {
                response = new SuccessResponse(resultRecord);
                res.json(response);
            } else {
                response = new FailureResponse("credential not found", err);
                res.json(response);
            }
        }).catch(function (err) {
            response = new FailureResponse("error deleting credential", err);
            res.json(response);
        });
    } catch (err) {
        response = new FailureResponse("error deleting credential", err);
        res.json(response);
    }


});
module.exports = router;
