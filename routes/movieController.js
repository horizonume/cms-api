var express = require('express');
var router = express.Router();
var movieModel = require("../models/movie");
var mongoseq = require("../utils/mongoseq");
var moviePrefix = "M";
var SuccessResponse = require('../models/out/successResp');
var FailureResponse = require('../models/out/failureResp');
var winston = require('winston');

/**
 *  GET ALL MOVIES 
 */
router.get('/all', function (req, res, next) {
  var response = {};
  movieModel.find({ status: "Active" }, function (err, data) {
    if (err) {
      response = new FailureResponse("Error Fetching Movies", err.errmsg);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * GET A MOVIE WITH GIVEN ID ":movie_id"
 */
router.get('/:movie_id', function (req, res, next) {
  var movieId = req.params.movie_id;
  var response = {};
  movieModel.findById({ "_id": movieId }, function (err, data) {
    if (err) {
      failureMessage = "error fetching movie with id " + movieId;
      response = new FailureResponse(failureMessage, err.errmsg);
    } else if (!data) {
      failureMessage = "error fetching movie details "
      failureReason = "Movie: " + movieId + " does not exist";
      response = new FailureResponse(failureMessage, failureReason);
    } else {
      response = new SuccessResponse(data);
    }
    res.json(response);
  });
});

/**
 * update movie
 */
router.post('/update', function (req, res, next) {
  var movieId = req.body.movieId;
  var updateMovie = {
    "_id": movieId,
    "movieName": req.body.movieName,
    "cast": {
      "heroes": req.body.hero,
      "heroines": req.body.heroine
    },
    "genre": req.body.genre,
    "banner": req.body.banner,
    "ratings": req.body.ratings,
    "avgRating": getAvgRating(req.body.ratings, movieId),
    "status": "Active"
  };
  var response = {};
  movieModel.findOneAndUpdate({ _id: movieId, "status": "Active" }, updateMovie, function (err, data) {
    if (err) {
      response = new FailureResponse("error updating movie", err.errmsg);
    } else if (!data) {
      var reason = "movie: " + movieId + " does not exist";
      response = new FailureResponse("error adding movie", reason);
    } else {
      var successMessage = "movie updated successfully";
      var updated = new Movie(movieId);
      response = new SuccessResponse(updated, successMessage);
    }
    res.json(response);
  });
});

/**
 * Create a new movie
 */
router.post('/add', function (req, res, next) {
  var nextSeqQuery = mongoseq("movieCounter");
  nextSeqQuery.exec(
    function (err, seqData) {
      if (err) {
        response = new FailureResponse("Error in Generating MovieId");
        res.json(response);
      } else {
        // Pad the generated sequence to generate a string of len 6
        // example : M00001, M00101, M10001
        var movieId = moviePrefix + ("00000" + seqData.seq).slice(-5);
        /* Creating New Movie Record */
        var newMovie = new movieModel({
          "_id": movieId,
          "movieName": req.body.movieName,
          "cast": {
            "heroes": req.body.hero,
            "heroines": req.body.heroine
          },
          "genre": req.body.genre,
          "banner": req.body.banner,
          "ratings": req.body.ratings,
          "avgRating": getAvgRating(req.body.ratings, movieId),
          "status": "Active"
        });
        var response = {};
        newMovie.save(function (err) {
          if (err) {
            response = new FailureResponse("error adding movie", err.errmsg);
          } else {
            var successMessage = "movie added successfully";
            var added = new Movie(movieId);
            response = new SuccessResponse(added, successMessage);
          }
          res.json(response);
        });
      }
    }
  );
});

router.post('/delete', function (req, res, next) {

  var movieId = req.body.movie_id;
  movieModel.findOneAndUpdate({ "_id": movieId, "status": "Active" }, { "status": "Deleted" }, function (err, data) {
    var response = {};
    if (err) {
      failureMessage = "Error Deleting Movie : " + movieId;
      response = new FailureResponse(failureMessage, err.errmsg);
    } else if (!data) {
      var reason = "movie: " + req.body.movieId + " does not exist";
      response = new FailureResponse("error deleting movie", reason);
    }
    else {
      var deleted = new Movie(movieId);
      successMessage = "Movie " + movieId + " deleted successfully";
      response = new SuccessResponse(deleted, successMessage);
    }
    res.json(response);
  });
});
function Movie(movieId) {
  this.movieId = movieId;
}

/**
 * Compute avgRating from reviews
 */
function getAvgRating(ratings, movieId) {
  var avgRating = 0;
  ratings.forEach(function (item) {
    if (isNaN(item.rating)) winston.log("warn", item.rating + " : rating is not a number, ignoring the rating");
    else avgRating = avgRating + item.rating;
  });
  try {
    avgRating = avgRating / (ratings.length);
  } catch (err) {
    winston.log("warn", err + ": Movie: " + movieId + " Rating defaulted 0");
  }
  return avgRating;
}

module.exports = router;
