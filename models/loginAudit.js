var mongoose = require("mongoose");

/**
 * Schema that stores successful logins.
 * TBD: Do we need audit collection for unsuccessful logins?
 * timestamps: true creates and takes care of updating the updatedAt field.
 * How do you update a loginRecord? There is no field to update in this schema.
 */
var loginAuditSchema = new mongoose.Schema({
    "_id": String,
    "userName": { type: String, index: true, unique: true },
    "role": String
}, { timestamps: true }); // timestamps: true, mongoose creates 
                         //  1. createdAt 
                         //  2. updatedAt fields 
                         //  and updates updated field during update of a record.

module.exports = mongoose.model('LoginAudit', loginAuditSchema);