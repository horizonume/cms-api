var mongoose = require("mongoose");


/**
 * Schema that stores employee login credentials
 */
var credentialSchema = new mongoose.Schema({
    "_id": String,
    "userName": { type: String, index: true, unique: true },
    "role": String,
    "password": String,
    "status": { type: String, default: "Active" }
}, { timestamps: true }); 

//  timestamps: true, mongoose creates 
//  1. createdAt 
//  2. updatedAt fields 
//  and updates updated field during update of a record.



module.exports = mongoose.model('Credential', credentialSchema);