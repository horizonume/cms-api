var mongoose = require("mongoose");
//mongoose.connect('mongodb://localhost:27017/cms');

//var mongoSchema =   mongoose.Schema;

var theatreItemSchema = {
    "_id": String,
    "theatreId": String,
    "theatreName": String,
    "itemCode": String, "itemName": String, "supplierName": String, "supplierContactName": String,
    "supplierContactNumber": String, "quantity": String, "perishable": Boolean,
    "status": String
};

module.exports = mongoose.model('TheatreItem', theatreItemSchema);

