var mongoose = require("mongoose");

var theatreMovieSchema = new mongoose.Schema({
    "movieId": {type: String, index: true},
    "movieName": String,
    "theatreId": {type: String, index: true},
    "theatreName": String,
    "screenName": String,
    "showTime": String,
    "fromDate": Date,
    "toDate": Date
}, { timestamps: true }); // timestamps: true, mongoose creates createdAt 
                         // and modifiedAt fields and updates them

theatreMovieSchema.index({ movieId: 1, theatreId: 1, screenName: 1, showTime: 1 }, { unique: true });


module.exports = mongoose.model('TheatreMovie', theatreMovieSchema);

