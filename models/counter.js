var mongoose = require("mongoose");
//mongoose.Promise = require('bluebird');

var counterSchema = new mongoose.Schema({
    _id: String,
    seq: {type: Number, default: 00001}
});
module.exports = mongoose.model('Counter',counterSchema);
