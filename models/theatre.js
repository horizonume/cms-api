var mongoose    =   require("mongoose");
//mongoose.connect('mongodb://localhost:27017/cms');

//var mongoSchema =   mongoose.Schema;

var theatreSchema  = {
    "_id" : String,
    "theatreName": String,
    "screens":  [{"name":String,"showTime":[]}],
    "address": {
		"doorNumber": String,
		"street1": String,
		"street2": String,
		"landmark": String,
		"city": String,
		"state": String,
		"country": String,
		"zipcode": String
	},
	"contact": {
		"mobileNumbers": [],
		"landlineNumber": String
    },
    "supervisors": [{"_id":String,"name":String,"contact":String}],
    "status" : String
};

module.exports = mongoose.model('Theatre',theatreSchema);

