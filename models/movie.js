var mongoose = require("mongoose");

var movieSchema = {
    "_id": String,
    "movieName": String,
    "cast": {
        "heroes": [String],
        "heroines": [String]
    },

    "genre": String,
    "banner": String,
    "ratings": [{"reviewer":String,"rating" : Number, _id : false }],
    "avgRating":Number,
    "status" : String
};
module.exports = mongoose.model('Movie', movieSchema);