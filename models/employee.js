var mongoose = require("mongoose");
var employeeSchema = {

	"_id": String,
	"companyName": String,
	"firstName": String,
	"lastName": String,
	"userName": String,
	"designation": String,
	"role": String,
	"homeAddress": {
		"houseNumber": String,
		"street1": String,
		"street2": String,
		"landmark": String,
		"city": String,
		"state": String,
		"country": String,
		"zipcode": String
	},
	"contact": {
		"mobileNumber": { type: String, unique: true },
		"landlineNumber": String
	},
	"status": String
}

module.exports = mongoose.model('Employee', employeeSchema);

