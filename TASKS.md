FUNCTIONAL
=======================================
ITEMS MASTER CRUD - DONE
EMPLOYEE MASTER CRUD + TESTS - PJ - DONE
    - Create - Unique Key constraint on phone number during - DONE
    - Update - Accept EmployeeID and all other fields and update the document. (Get Followed by Update) - DONE
    - Delete - Employee Document - Change status from Active to Deleted   - DONE
    - TEST CASES
THEATRE MASTER CRUD + TESTS - Neelima
MOVIES CRUD + TESTS - PJ - DONE
THEATRE VS ITEM - NEELIMA DONE
MOVIE VS THEATRE - PJ DONE
LOGIN CONTROLLER - PJ - IN PROGRESS
     credential util
        create - DONE
        update - TBD
     create credential on employee creation - DONE
     login audit - TBD
Login Screen with angular.js
SALE AUDIT - TBD








NON-FUNCTIONAL
=======================================
SEQUENCE operation - DONE
Unit Tests - mocha - PJ - DONE
Logging in node.js - WINSTON MODULE -  Neelima/PJ - DONE
Logging HTTP Request - MORGAN MODULE - DONE
Log Rotation - TBD
Create a template for Request, Response Objects - DONE
BLUEBIRD promise library for mongoose queries - DONE
Angular 1.0 vs 2.0 - PJ - In Progress